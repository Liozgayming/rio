package ch.bbbaden.rio;

public class Spiel {
    private int aktuellerSpielerIndex;
    private final Gui gui;
    private final Rio rio;
    private final Spieler[] spieler;


    public Spiel(Spieler[] spieler, Rio rio, Gui gui){
        this.spieler = spieler;
        this.rio = rio;
        this.gui = gui;
    }

    public void spiele(){
        waehleJungstenSpieler();
        while (!spielIstFertig()){
            if (rio.play()){
                spieler[aktuellerSpielerIndex].gibStaebchen();
            } else {
                spieler[aktuellerSpielerIndex].nimmStaebchen();
            }
           gui.printRio(rio.getLoecher());
            gui.printSpieler(spieler);
            waehleNaechstenSpieler();
        }
    }

    private boolean spielIstFertig(){
        for (int i = 0; i < spieler.length; i++) {
            if (spieler[i].getAnzahlStaebchen() == 0){
                gui.printGewinner(spieler[i]);
                return true;
            }
        }
        return false;
    }

    private void waehleJungstenSpieler(){
        aktuellerSpielerIndex = 0;
        for (int i = 1; i < spieler.length; i++) {
            if (spieler[aktuellerSpielerIndex].getAlter() > spieler[i].getAlter()){
                aktuellerSpielerIndex = i;
            }
        }
    }
    private void waehleNaechstenSpieler(){
        if (aktuellerSpielerIndex == spieler.length -1){
            aktuellerSpielerIndex = 0;
        }else {
            aktuellerSpielerIndex++;
        }
    }
}
