package ch.bbbaden.rio;

import java.util.Random;

public class RealerWuerfel implements Wuerfel{

    private static final int MAX_NUMBER = 6;

    @Override
    public int wuerfle() {
        Random random = new Random();
        return random.nextInt(MAX_NUMBER);
    }
}
