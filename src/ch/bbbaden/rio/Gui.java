package ch.bbbaden.rio;

public class Gui {
    public void printRio(boolean[] loecher){
        System.out.println("__Rio__");
        for (int i = 0; i < loecher.length; i++) {
            if (loecher[i]){
            System.out.println("|  o  |");
            } else{
                System.out.println("|  *  |");
            }
        }
        System.out.println("_______");
    }
    public void printSpieler(Spieler[] spieler){
        for (int i = 0; i < spieler.length; i++) {
            System.out.println("Name: " + spieler[i].getName() + " Stäbchen: " + spieler[i].getAnzahlStaebchen());
        }
    }

    public void printGewinner(Spieler spieler){
        System.out.println("Spieler: " + spieler.getName() + " hat gewonnen.");
    }
}
