package ch.bbbaden.rio;

public class Main {

    public static void main(String[] args) {

        Spieler[] spieler = new Spieler[]{
                new Spieler("Micha", 16, 6),
                new Spieler("Joel", 18, 6)
        };
        Spiel spiel = new Spiel(spieler, new Rio(new RealerWuerfel()), new Gui());
        spiel.spiele();
    }
}

