package ch.bbbaden.rio;

public class Rio {
    private static final int MAX = 6;
    private boolean[] loecher = new boolean[MAX];
    private Wuerfel wuerfel;

    public Rio(Wuerfel wuerfel){
        this.wuerfel = wuerfel;
    }
    public boolean play(){
        int wuerfelNumber = wuerfel.wuerfle();
        if (wuerfelNumber == MAX){
            return  true;
        }
        loecher[wuerfelNumber] = !loecher[wuerfelNumber];
        return loecher[wuerfelNumber];
    }

    public boolean[] getLoecher() {
        return loecher;
    }
}
