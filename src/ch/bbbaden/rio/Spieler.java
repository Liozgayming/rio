package ch.bbbaden.rio;

public class Spieler {

    private int anzahlStaebchen;
    private final String name;
    private final int alter;

    public Spieler(String name, int alter, int anzahlStaebchen){
        this.name = name;
        this.alter = alter;
        this.anzahlStaebchen = anzahlStaebchen;
    }

    public void nimmStaebchen(){
        anzahlStaebchen++;
    }
    public void gibStaebchen(){
        anzahlStaebchen--;
    }

    public int getAlter() {
        return alter;
    }

    public String getName() {
        return name;
    }

    public int getAnzahlStaebchen() {
        return anzahlStaebchen;
    }
}
