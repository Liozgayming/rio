package ch.bbbaden.rio;

public class WuerfelMock implements Wuerfel{
    private final int wurf;
    public WuerfelMock(int wurf){
        this.wurf = wurf;
    }
    @Override
    public int wuerfle() {
        return wurf;
    }
}
